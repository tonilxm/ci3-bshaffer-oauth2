<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
        parent::__construct();
        $this->load->library("oauthserver");
    }

	public function index()
	{
		$this->load->view('welcome_message');
	}

    /**
     * Sample:
        curl -X GET http://localhost:83/welcome/safe_access \
        -H 'Authorization: Bearer 10af9efcb3d423546134a96816e98e1c7dcd89b76' \
        -H 'cache-control: no-cache' \
     */
    public function safe_access()
    {
        $this->oauthserver->require_scope();

        $this->load->view('safe_access');
    }
}
