<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OAuth extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library("oauthserver");
    }

    /**
     * Usage sample:
     * curl -X POST http://localhost:83/oAuth/client_credentials -H 'Content-Type: application/x-www-form-urlencoded' \
            -H 'cache-control: no-cache' -d 'grant_type=client_credentials&client_id=test_client&client_secret=testpass'
     */
    public function client_credentials()
    {
        $this->oauthserver->client_credentials();
    }

    /**
     * Sample : curl -X POST http://localhost:83/oAuth/password_credentials -H 'Content-Type: application/x-www-form-urlencoded'
        -H 'cache-control: no-cache'
        -d 'grant_type=password&client_id=test_password&client_secret=testpass&username=toni&password=testpass'
     */
    public function password_credentials()
    {
        $this->oauthserver->password_credentials();
    }

    public function refresh_token() {
        $this->oauthserver->refresh_token();
    }

}
