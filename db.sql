-- MySQL dump 10.13  Distrib 5.7.25, for macos10.14 (x86_64)
--
-- Host: localhost    Database: oauth2
-- ------------------------------------------------------
-- Server version	5.7.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('07fabd8b3df46d5541f2711cb38f53750d8e841c','testclient',NULL,'2019-03-07 04:53:29',NULL),('10e3d6da80ecc9c8badccc807a746c30cb945cd0','testclient',NULL,'2019-03-08 00:11:12',NULL),('132e2c2ab79a0f0da04d00e798ea1b9221e70fbf','testclient',NULL,'2019-03-07 04:53:40',NULL),('190585bfec7ea774ab442ce7521637d5972f9aca','testclient',NULL,'2019-03-08 00:28:57',NULL),('1c140eafecee20327833da3d34bb893c64b97635','testclient',NULL,'2019-03-08 00:33:56',NULL),('27dc460cc882006c6bc8869d2cabe03e94141beb','test_password','toni','2019-03-09 03:31:14',NULL),('325a8a198bd76e18babd299f3de621b257172160','testclient',NULL,'2019-03-07 23:26:58',NULL),('35d2a5173385fd4d2086e651cb1d51d781e344ab','test_password','toni','2019-03-09 03:14:20',NULL),('3c14d13f74d7cba9d09201cd988a2ab1cc9444a9','testclient',NULL,'2019-03-08 00:15:12',NULL),('42b5a7b4516eab6b52e43dbd1896a8fe6f37e8e2','testclient',NULL,'2019-03-07 23:35:28',NULL),('4401bd5dfdee2c5a1a185b3261dadc9165767972','testclient',NULL,'2019-03-08 00:31:33',NULL),('4a708fc311e03365d78bb41f16bacf0e35ef5e6f','test_password','toni','2019-03-09 03:04:26',NULL),('5c0bdad7b92f06f4d61b588e198aa2046843477b','testclient',NULL,'2019-03-08 08:50:38',NULL),('63347433f62c15f5d636f0f4778e5a1499594d7a','testclient',NULL,'2019-03-07 23:28:42',NULL),('7fc8e3bed2e741b26c05ac70dec01b9270448283','testclient',NULL,'2019-03-07 22:30:39',NULL),('81e031b01a3418733862bfa384bfcc59963abd64','testclient',NULL,'2019-03-08 00:02:13',NULL),('82b3bfada27f5d329f3755ef3c19dc20c2730c89','test_client',NULL,'2019-03-09 02:03:58',NULL),('88b9eda3be71a9bf6b6fffa4aa416f0784af8c0d','testclient',NULL,'2019-03-08 00:37:22',NULL),('924164f8efaf9ce48dca0132b4e09df8064e4c3e','testclient',NULL,'2019-03-08 00:25:07',NULL),('92ae295268dedbed2a62a66490ac518e39b89fe7','testclient',NULL,'2019-03-08 00:40:12',NULL),('975bf791b9acdedd0e87f3f01f51f8e1599506ca','testclient',NULL,'2019-03-08 00:14:16',NULL),('977e17eb785056fa1d2496b681a9eb6d93dbfdcb','test_password','toni','2019-03-09 03:18:38',NULL),('983796f96e256f3c47aa6c04ac4d6e02b9230307','testclient',NULL,'2019-03-07 22:33:20',NULL),('9a1e3871e54578dee760bc3964b3b861a6bffd2f','testclient',NULL,'2019-03-07 22:42:41',NULL),('a287df7b2d7c399831cca3be47334d96c814ad49','testclient',NULL,'2019-03-07 23:27:32',NULL),('a7aa0da565abc525424dee1ef37d74e3df6325c7','testclient','testclient','2019-03-08 09:18:56',NULL),('a9841e2dbc00a0c085e45cc7d0d56d4800a0488c','test_password','toni','2019-03-09 02:47:25',NULL),('b06c019ddc0547c274b0b57d76ddc938a7b16bec','test_client',NULL,'2019-03-09 03:14:17',NULL),('b4039eb97e9691d3369f7407a1d72070d369cb3e','testclient',NULL,'2019-03-07 22:33:44',NULL),('b6aad201aa4a367b9198905f6016a5374c590bc4','test_password','toni','2019-03-09 03:07:21',NULL),('b7891096a72abfb54d2a82ef4a2be6d2c0c73497','testclient',NULL,'2019-03-07 23:35:45',NULL),('bcb3143209f1b7262720308926bef6b2c259a558','testclient',NULL,'2019-03-08 00:12:14',NULL),('c07bb423e17c1be363b133a53e6e8fd3ee360b56','test_password','toni','2019-03-09 02:35:42',NULL),('c7857e2a90c05e62d737dce023d7f17d49447b1c','test_password','toni','2019-03-09 03:31:21',NULL),('cbf1c52564843fef112b09458ef6def172352954','testclient',NULL,'2019-03-07 22:54:16',NULL),('d23910bc1f0d4bb687820705f02266abaa908759','test_password','toni','2019-03-09 03:03:55',NULL),('d2850f5c8f30d5cae46aa3b33dec92ea6910b510','testclient',NULL,'2019-03-08 00:37:53',NULL),('d7707e353136e77203d5956d3d04aaf58c962aa3','testclient',NULL,'2019-03-08 00:34:36',NULL),('e1c5638cbb41583e385ae92e8a3bbf6aca276d31','test_password','toni','2019-03-09 02:36:14',NULL),('e47966b368ca428ae2952bd61de35f7a28930ee3','test_password','toni','2019-03-09 03:15:13',NULL),('e82b11b5852d77bf0e150e48fba9ed540ada6e01','test_password','toni','2019-03-09 02:37:34',NULL),('eac3528bf2821402963e505b921bff377b594956','testclient',NULL,'2019-03-07 05:01:46',NULL),('f2e378fb60c50cc80b88010a94022c8774694fc9','testclient',NULL,'2019-03-07 22:44:09',NULL),('f4b5c36eab1a9099a5b25dc23bdcba7bd5abc9b0','testclient','testclient','2019-03-08 09:31:40',NULL),('f9f3fa1c97dec8a7c2db2e47f3863bc80688e3c5','testclient',NULL,'2019-03-07 22:34:06',NULL);
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_authorization_codes`
--

DROP TABLE IF EXISTS `oauth_authorization_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(4000) DEFAULT NULL,
  `id_token` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_authorization_codes`
--

LOCK TABLES `oauth_authorization_codes` WRITE;
/*!40000 ALTER TABLE `oauth_authorization_codes` DISABLE KEYS */;
INSERT INTO `oauth_authorization_codes` VALUES ('9acf039111063808d54295fca976146933d3db22','testclient',NULL,NULL,'2019-03-07 03:57:02',NULL,NULL);
/*!40000 ALTER TABLE `oauth_authorization_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `scope` varchar(4000) DEFAULT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES ('test_client','testpass','http://fake/','client_credentials',NULL,NULL),('test_password','testpass','','password refresh_token',NULL,NULL);
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_jwt`
--

DROP TABLE IF EXISTS `oauth_jwt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) NOT NULL,
  `subject` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_jwt`
--

LOCK TABLES `oauth_jwt` WRITE;
/*!40000 ALTER TABLE `oauth_jwt` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_jwt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('088ea0de1b8eb883e5e3018bba3175075d802351','test_password','toni','2019-03-23 02:07:21',NULL),('282c3b5524aca0748dd19616cf9880afd87f9ed9','test_password','toni','2019-03-23 02:04:26',NULL),('31ceb52e8a87321cbe1b3d885913a2d91f3878f7','testclient',NULL,'2019-03-21 04:01:46',NULL),('31ef048c3ee3bde184fb6b9b401a0080cc56b560','testclient','testclient','2019-03-22 08:31:40',NULL),('33ba3742884a654fff761b9d0dcb19a49377ed24','testclient','testclient','2019-03-22 08:18:56',NULL),('49a1e7bae422b98872479e7bf9bd431e32ab5ead','test_password','toni','2019-03-23 01:35:42',NULL),('6ee7fe76afc3390c178a70b2879b64b6e2113fcd','testclient',NULL,'2019-03-21 23:40:12',NULL),('76e1ec63c8c0f89f318f6adffb153ffd9b6710c8','testclient',NULL,'2019-03-21 23:34:36',NULL),('985024ddbb5d6a677d2749a2e86cdbda24c952a1','testclient',NULL,'2019-03-21 23:31:33',NULL),('9f5296e0bb4fd2768412d246f8da4cc053f6d771','test_password','toni','2019-03-23 01:36:14',NULL),('c03aa198207ef9fc0ec307a2d9a6fe609fc90b38','test_password','toni','2019-03-23 02:31:21',NULL),('d4534a3c99881d467b0feae6cf1ebf616219f20a','test_password','toni','2019-03-23 01:47:25',NULL),('df6956685fa8a7a53154de9de62af745e7109bac','test_password','toni','2019-03-23 02:14:20',NULL),('e4531e2607c0c6192b04ba0012f67050ef41231f','test_password','toni','2019-03-23 01:37:34',NULL),('eed7deb9002268bcd66ad467b4048dd5f82f8516','test_password','toni','2019-03-23 02:31:14',NULL),('eee40c6806eaa101252d0f8769d2d6bb04a4bde2','test_password','toni','2019-03-23 02:15:13',NULL),('f9bef852a77f811c65735da9ff062327fc13ed43','test_password','toni','2019-03-23 02:03:55',NULL);
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_scopes`
--

DROP TABLE IF EXISTS `oauth_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_scopes` (
  `scope` varchar(80) NOT NULL,
  `is_default` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`scope`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_scopes`
--

LOCK TABLES `oauth_scopes` WRITE;
/*!40000 ALTER TABLE `oauth_scopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_users`
--

DROP TABLE IF EXISTS `oauth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_users` (
  `username` varchar(80) NOT NULL,
  `password` varchar(80) DEFAULT NULL,
  `first_name` varchar(80) DEFAULT NULL,
  `last_name` varchar(80) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `email_verified` tinyint(1) DEFAULT NULL,
  `scope` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_users`
--

LOCK TABLES `oauth_users` WRITE;
/*!40000 ALTER TABLE `oauth_users` DISABLE KEYS */;
INSERT INTO `oauth_users` VALUES ('toni','206c80413b9a96c1312cc346b7d2517b84463edd','Toni','Liem','toni.liem@gmail.com',1,NULL);
/*!40000 ALTER TABLE `oauth_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-09  9:36:34
